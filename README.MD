# Advanced Programming

## Introduction to Git, Gitlab and Gitlab CI

@author ggordonutech

### Objectives

- Git Flow
- Gitlab
  - Issues
  - Settings
- Gitlab CI

### Overview

This introduction to Gitlab CI uses a simple bash program to demonstrate Continuous Deployment with Gitlab CI (build/test/package)

Initial program adds two numbers. Example run to add 5 + 3:


    ./program.sh 5 3

with expected output 


    Using default output file name of 'output.txt'.
    We will attempt to add 5 + 3
    CALCULATION : 5 + 3 = 8


and by default should create a file named "ouput.txt" with the following contents:

    We have done a lot of work
    CALCULATION : 5 + 3 = 8


If you would like to change the output file, simply specify the new output file name in an environment variable named $OUTPUT___FILE___NAME. Example

    export OUTPUT_FILE_NAME="results.txt"

### Items of Interest

- .gitlab-ci.yml <-- Gitlab Configuration File
- program.sh <-- bash program

### Additional References and Resources

See ```program.sh``` and ```.gitlab-ci.yml``` for additional comments

- Detailed Documentation is available at https://docs.gitlab.com/ee/ci/yaml/
- Examples for different languages and frameworks https://docs.gitlab.com/ee/ci/examples/

